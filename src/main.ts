import { enableProdMode } from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import { AppComponent } from './app/app.component';

// Other imports or code may be here (e.g., environment checks)

bootstrapApplication(AppComponent).catch(err => console.error(err));
