import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WoocommerceService {
  private baseUrl = 'https://woocommerce/wp-json/wc/v3/products';
  private consumerKey = 'ck_5f01a308e1364ac46f4f7d2602fbb5941086bca7';
  private consumerSecret = 'cs_35922740526a47a404b56fa364c4c191693e7356';

  constructor(private http: HttpClient) { }

  getProducts(): Observable<any> {
    const url = `${this.baseUrl}?consumer_key=${this.consumerKey}&consumer_secret=${this.consumerSecret}`;
    return this.http.get(url);
  }
}
