import { Component, OnInit } from '@angular/core';
import { WoocommerceService } from '../woocommerce.service';

@Component({
  selector: 'app-product-list',
  standalone: true,
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})

export class ProductListComponent implements OnInit {
  products: any[] = [];

  constructor(private woocommerceService: WoocommerceService) { }

  ngOnInit(): void {
    this.woocommerceService.getProducts().subscribe(
      (data) => {
        this.products = data;
      },
      (error) => {
        console.error('Error fetching products', error);
      }
    );
  }
}
